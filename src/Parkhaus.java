import java.util.ArrayList;
import java.util.List;

public class Parkhaus {
    public int maxKapazitaet;
    private int freieParkplaetze;
    List<Object> parkplaetze=new ArrayList<>();

    public Parkhaus(int maxKapazitaet) {
        this.maxKapazitaet = maxKapazitaet;
        this.freieParkplaetze = maxKapazitaet;
    }

    public int getFreieParkplaetze() {
        return freieParkplaetze;
    }

    public int einfahren(Auto auto) {
            this.parkscheinZiehen();
            parkplaetze.add(auto);
            auto.setParkhaus(this);
            freieParkplaetze=maxKapazitaet-parkplaetze.size();
            auto.fahren();
            auto.bremsen();
            auto.MotorAus();
            return freieParkplaetze;
        }

    private void parkscheinZiehen() {
        System.out.println("Bitte");
    }

    public int ausfahren(Auto auto) {
        auto.MotorAn();
        auto.fahren();
        this.parkscheinLoesen();
        parkplaetze.remove(auto);
        auto.setParkhaus(null);
        freieParkplaetze=maxKapazitaet-parkplaetze.size();
        return freieParkplaetze;
    }

    private void parkscheinLoesen() {
        System.out.println("Danke");
    }
}
