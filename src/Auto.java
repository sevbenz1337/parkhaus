import java.util.Date;

public class Auto {
    //Auto-Bezeichnung
    public String Marke;
    public String Typ;
    private Parkhaus parkhaus;



    public Auto(String marke, String typ) {
        this.Marke = marke;
        this.Typ = typ;
    }

    public Parkhaus getParkhaus() {
        return parkhaus;
    }

    public void setParkhaus(Parkhaus parkhaus) {
        if (this.parkhaus!=null) {
            System.out.println("Auto bereits geparkt");
        } else {
            this.parkhaus = parkhaus;
        }
    }

    public void MotorAn() {
        System.out.print("Motor startet");
        System.out.println();
    }

    public void MotorAus() {
        System.out.print("Motor aus");
        System.out.println();
    }

    public void fahren() {
        System.out.print("Auto fährt");
        System.out.println();
    }

    public void bremsen() {
        System.out.print("Auto bremst");
        System.out.println();
    }
}
