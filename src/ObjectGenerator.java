import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
//import java.lang.reflect.Method;
//import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


//Klasse um mit java.lang.reflect Methoden Klassen zu erstellen

public class ObjectGenerator {
    public Class<?> cls;
    public List<List<String>> list;                                                                                     //2D-Liste des Inhalts
    public String typ;                                                                                                  //Klassen-Name
    public String dateiName;                                                                                            //Datei-Name
    public WriteCSV wCSV;

    public ObjectGenerator(Class<?> cls) {
        this.typ = cls.getName();                                                                                       //Klassen-Name schreiben
        this.dateiName=typ+".csv";                                                                                      //Datei-Name schreiben
        this.cls = cls;                                                                                                 //Klasse schreiben
        this.wCSV=new WriteCSV(cls);
        this.list=wCSV.getList(dateiName);                                                                              //2D-Liste des Inhalts schreiben
    }

    public Object makeObject(int instance) {                                                                            //makeObject erstellt eine Instanz der übergebenen Klasse aus den Werten der csv-Datei
        Class cl = null;
        try {
            cl = Class.forName(cls.getName());                                                                          //Ermittlung des Klassennamens
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        int m = wCSV.AttributeFinder().size();                                                                          //Menge der zu setzenden Attribute

        Class[] partypes =new Class[m];                                                                                 //Array um die Parametertypen zu speichern
        Object[] arglist = new Object[m];                                                                               //Object der Argumente
        Field[] fields =  cls.getDeclaredFields();                                                                      //Erstellen des Felds der Felder der Klasse
        int j=0;

        for (Field field:fields) {                                                                                      //Durchlaufen aller Elemente
            String string = field.toString();                                                                           //Erzeugen eines String Elements
            boolean aPublic = string.startsWith("public");                                                              //Bool-Variable ob Klassen-Feld ein Public-Attribut ist
            if (aPublic) {
                if (string.contains("String")) {                                                                        //ist das Klassenattribut ein String
                    partypes[j] = String.class;                                                                         //Parametertyp auf String setzen
                    arglist[j] = list.get(instance).get(j+1);                                                           //Wert des Attributs
                }
                if (string.contains("int")) {                                                                           //ist das Klassenattribut ein Integer
                    partypes[j] = Integer.TYPE;                                                                         //Parametertyp auf Integer setzen
                    arglist[j] = Integer.valueOf(list.get(instance).get(j+1));                                          //Wert des Attributs
                }
                j++;
            }
        }


        Constructor ct = null;                                                                                          //Initialisieren des Konstruktors
        try {
            if (cl != null) {
                ct = cl.getConstructor(partypes);                                                                           //Konstruktor der übergebenen Klasse setzen und Parameterrampe übergeben
            }
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }

        Object retobj = null;                                                                                           //Initialisieren des Rückgabe-Objektes
        try {
            if (ct != null) {
                retobj = ct.newInstance(arglist);                                                                           //Neue Instanz mit den Attributen des Objekts erzeugen
            }
        } catch (InstantiationException | IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
        }
        return retobj;                                                                                                  //Rückgabe des Objekts
    }


    public HashMap mapInstances() {                                                                                     //mapInstances um eine HashMap aller aus der csv-Datei zu erzeugenden Objekte zu speichern
        int a = list.size()-1;                                                                                            //Zahl der zu erzeugenden Instanzen
        System.out.println(typ+"-DB Einträge: " + a);                                                                   //Ausgabe der Anzahl der zu erzeugenden Instanzen

        Map<String, Object> Hash = new HashMap<>();                                                                     //neue HashMap um Objekte zu speichern

        for (int i = 1; a >= i; i++) {                                                                                   //Erzeugung aller Objekte (Beginn 2. Zeile in der csv-Datei)
            Hash.put(list.get(i).get(0), makeObject(i));                                                                //Name des Objekts und zugehöriges Objekt speichern
        }

        return (HashMap) Hash;                                                                                          //Ausgabe der HashMap
    }
/*
    public static Object runGetter(Field field, Class<?> o)                                                             //ToDo: Identifizierung der Attribute über public ersetzen.
    {
        // MZ: Find the correct method
        for (Method method : o.getMethods())
        {
            if ((method.getName().startsWith("get")) && (method.getName().length() == (field.getName().length() + 3)))
            {
                if (method.getName().toLowerCase().endsWith(field.getName().toLowerCase()))
                {
                    // MZ: Method found, run it
                    try
                    {
                        return method.invoke(o);
                    }
                    catch (IllegalAccessException e)
                    {
                        // Logger.fatal("Could not determine method: " + method.getName());
                    }
                    catch (InvocationTargetException e)
                    {
                        // Logger.fatal("Could not determine method: " + method.getName());
                    }

                }
            }
        }


        return null;
    }
*/
}
