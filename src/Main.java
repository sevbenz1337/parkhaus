import java.util.HashMap;

public class Main {

    public static void main(String[] args)  {

        DB dbAuto = new DB(Auto.class);
        DB dbParkhaus = new DB(Parkhaus.class);

        HashMap autoMap =dbAuto.DB_config();
        HashMap parkhausMap =dbParkhaus.DB_config();


        boolean exit = true;

        do  {
            System.out.println("\"0\" um Auto zu parken");
            System.out.println("\"1\" um Auto auszuparken");
            System.out.println("\"2\" um Autos anzuzeigen");
            System.out.println("\"3\" um Parkhäuser anzuzeigen");
            System.out.println("\"4\" um Programm zu verlassen");

            int input = Tools.intEingabe();
            switch (input) {
                case 0 -> {
                    System.out.println("Gib das Auto ein, das geparkt werden soll:");
                    String a = Tools.stringEingabe();
                    System.out.println("Gib das Parkhaus ein, in dem geparkt werden soll:");
                    String p = Tools.stringEingabe();

                    Auto auto = (Auto) autoMap.get(a);
                    Parkhaus parkhaus = (Parkhaus) parkhausMap.get(p);
                    parkhaus.einfahren(auto);
                }
                case 1 -> {
                    System.out.println("Gib das Auto ein, das ausgeparkt werden soll:");
                    String a = Tools.stringEingabe();
                    Auto auto = (Auto) autoMap.get(a);
                    Parkhaus parkhaus=auto.getParkhaus();
                    if (parkhaus!=null) {
                        parkhaus.ausfahren(auto);
                    } else {
                        System.out.println("Auto ist nicht geparkt");
                    }
                }
                case 2 -> {
                    for (Object v : autoMap.keySet()) {                                                                             //Ausgabe der Autos
                        Auto auto = (Auto) autoMap.get(v);
                        System.out.println("Name: "+v+" Marke: "+auto.Marke+"   Typ: "+auto.Typ);
                        System.out.println();
                    }
                }
                case 3 -> {
                    for (Object v : parkhausMap.keySet()) {                                                                         //Ausgabe der Parkhäuser
                        Parkhaus parkhaus = (Parkhaus) parkhausMap.get(v);
                        System.out.println("Name: "+v+" Parkplätze: "+parkhaus.maxKapazitaet+" freie Parkplätze: "+parkhaus.getFreieParkplaetze());
                        System.out.println();
                    }

                }
                case 4 -> exit = false;
            }
        }while(exit);
    }
}
