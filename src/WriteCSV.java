import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.*;
import java.util.*;

//Klasse um die Daten der Objekte in csv-Dateien zu speichern und zu verwalten.

public class WriteCSV {


    public List<List<String>> list;                                                                                     //2D-Liste des Inhalts
    public String typ;                                                                                                  //Klassen-Name
    public String dateiName;                                                                                            //Datei-Name
    public Class<?> cls;                                                                                                //Klasse des zugehörigen Objects


    public WriteCSV(Class<?> cls) {

        this.typ = cls.getName();                                                                                       //Klassen-Name schreiben
        this.dateiName=typ+".csv";                                                                                      //Datei-Name schreiben
        this.cls = cls;                                                                                                 //Klasse schreiben
        this.list=getList(dateiName);                                                                                   //2D-Liste des Inhalts schreiben
    }
    private List<String> getRecordFromLine(String line) {                                                               //getRecordFromLine um eine Liste der Einträge einer Zeile zu erstellen
        List<String> values = new ArrayList<>();                                                                        //Listenobjekt der Zeile
        try (Scanner rowScanner = new Scanner(line)) {
            rowScanner.useDelimiter(";");                                                                               //Trennzeichen ";"
            while (rowScanner.hasNext()) {                                                                              //Solange es einen weiteren Eintrag in der Zeile gibt
                values.add(rowScanner.next());                                                                          //Eintrag in das Listenobjekt schreiben
            }
        }
        return values;                                                                                                  //Ausgabe des Listenobjekts der Zeile
    }
    public List<List<String>> getList(String Name)  {                                                                   //getList erzeugt eine 2D-Liste aus den Zeileninhalten der csv-Datei
        List<List<String>> data = new ArrayList<>();                                                                    //lokales Objekt zum Erzeugen der 2D-Liste
        Scanner scanner = null;
        try {
            scanner = new Scanner(new File(Name));                                                                      //Scanner auf erzeugten File-Objekt erzeugen
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        if (scanner != null) {                                                                                          //Check, ob die Erzeugung des Scanner-Objektes erfolgreich war
            while (scanner.hasNextLine()) {                                                                             //solange neue Zeile existiert
                data.add(getRecordFromLine(scanner.nextLine()));                                                        //Daten der neuen Zeile in 2D-Liste schreiben
            }
        }
        return data;
    }

    public void CreateFile() throws IOException {                                                                       //CreateFile um eine csv-Datei mit zur Klasse passenden Spalten zu erzeugen
        FileWriter csvWriter = null;                                                                                    //Initialisieren des FileWriters
        try {
            csvWriter = new FileWriter(dateiName, false);                                                        //Überschreiben der Datei
        } catch (IOException e) {
            e.printStackTrace();
        }
        StringBuilder newLine = new StringBuilder();                                                                    //Erstellen des StringBuilders für die Kopfzeile der Tabelle
        newLine.append("Key");                                                                                          //Erste Spalte ist der Key des Objekts (später auch die Referenz), hinzufügen zum StringBuilder
        List<String> attribute =AttributeFinder();                                                                      //AttributeFinder erstellte eine Liste der public-Attribute der Klasse (Argumente des Konstruktors)
        for (String att:attribute){
                newLine.append(";").append(att);                                                                        //die Attribute werden der Kopfzeile hinzugefügt
            }
        assert csvWriter != null;
        csvWriter.append(String.valueOf(newLine)).append("\n");                                                         //Übergabe der Kopfzeile an csvWriter
        try {
            csvWriter.flush();                                                                                          //Schreiben der Zeile
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        try {
            csvWriter.close();                                                                                          //Beenden des Dateizugriffs
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    public void FileExists() {                                                                                          //FileExists testet, ob csv-Datei existiert und mit geeigneter Kopfzeile erstellt ist. *Schlechte Lösung, direkt über FileWriter lösen und nicht über File
        File temp = new File(dateiName);                                                                                //Erzeugen eines temporären File-Objekts mit entsprechenden Namen
        int m=AttributeFinder().size();                                                                                 //Anzahl der Public Attribute der Klasse
        try {
            if (temp.createNewFile()) {                                                                                 //die Datei konnte neu erzeugt werden
                System.out.println("DB-"+typ+" erzeugt (\"" + temp.getName()+"\")");                                    //Ausgabe: "DB-Beispiel erzeugt "Beispiel.csv""
                CreateFile();                                                                                           //Überschreiben des Datei-Inhalts mit der korrekten Kopfzeile
            } else {                                                                                                    //Datei mit dem Dateinamen existiert bereits
                System.out.println("DB-"+typ+" existiert (\""+dateiName+"\")");                                         //Ausgabe: "DB-Beispiel existiert "Beispiel.csv""
                if(list.size()!=0) {                                                                                    //Datei nicht leer
                    List<String> subList=list.get(0);                                                                   //Liste der Spalten-Titel *Erweitern auf Vergleich aller Einträge
                    int n=subList.size();                                                                               //Anzahl der Spalten-Titel
                    if (n==(m+1)) {                                                                                     //Vergleich der Spalten-Titel (=Schlüssel+Attribute) mit den Attributen der Klasse
                        System.out.println("Existierende Datei ok");                                                    //Datei ist scheinbar in Ordnung
                    } else {
                        System.out.println("Existierende Datei nicht ok, wird neu erzeugt");                            //Datei ist nicht in Ordnung
                        CreateFile();                                                                                   //Überschreiben des Datei-Inhalts mit der korrekten Kopfzeile *Option zur Ausgabe des aktuellen Inhalts
                    }
                }
                 else {
                    CreateFile();                                                                                       //Die Datei ist leer: Schreiben der korrekten Kopfzeile
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public List<String>  AttributeFinder() {                                                                            //AttributeFinder erstellt eine Liste der public Attribute der Klasse
        List<String> attribute= new ArrayList<>();                                                                      //Anlage der Liste
        Field[] fields =  cls.getDeclaredFields();                                                                      //Erstellen des Felds der Felder der Klasse
        for (Field field:fields) {                                                                                      //Durchlaufen aller Elemente
            String string=field.toString();                                                                             //Erzeugen eines String Elements
            boolean aPublic = string.startsWith("public");                                                              //Bool-Variable ob Klassen-Feld ein Public-Attribut ist
            if (aPublic) {
                attribute.add(field.getName());                                                                         //Public-Attribute der Klasse hinzufügen
            }
        }
        return attribute;
    }

    public void ShowItems() {                                                                                           //ShowItems zur Ausgabe des Tabellen-Inhalts *verschönern
        int a = list.size();
        System.out.println(a);
        System.out.println(list);
    }

    public void AddItem()  {                                                                                            //AddItem fügt eine Zeile der entsprechenden Tabelle hinzu
        FileWriter csvWriter = null;                                                                                    //Initialisieren des FileWriter
        try {
            csvWriter = new FileWriter(dateiName, true);                                                         //neuer FileWriter - hinzufügen von Inhalten der Datei
        } catch (IOException e) {
            e.printStackTrace();
        }
        StringBuilder newLine = new StringBuilder();                                                                    //neuer StringBuilder um neue Zeile zu erzeugen
        newLine.append(typ).append(list.size());                                                                        //erster Eintrag ist der Key des Objekts (Klassen-Name und Nummer des Eintrags). Die Elemente sind geordnet eingetragen
        String key= newLine.toString();                                                                                 //Zwischenspeichern des Key
        List<String> Attribute =AttributeFinder();                                                                      //AttributeFinder erstellte eine Liste der public-Attribute der Klasse (Argumente des Konstruktors)
        for (String att:Attribute){                                                                                     //Schleife durch alle Public-Attribute der Klasse
            System.out.println("Eingabe " + att + " von " + typ);                                                       //Ausgabe zur Abfrage des Attributs
            newLine.append(";").append(Tools.stringEingabe());                                                          //Hinzufügen des Eingabe-Werts
        }
        System.out.println("Das neue Element heißt: " + key);                                                           //Ausgabe des Keys des neuen Elements
        try {
            assert csvWriter != null;
            csvWriter.append(String.valueOf(newLine)).append("\n");                                                     //Übergabe der Zeile an csvWriter
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        try {
            csvWriter.flush();                                                                                          //Schreiben der Zeile
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        try {
            csvWriter.close();                                                                                          //Beenden des Datei-Zugriffs
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        this.ShowItems();                                                                                               //Anzeige der neuen Inhalte der Tabelle
    }

    public void DeleteItem(String delete)  {                                                                            //DeleteItem löscht einen Eintrag und erstellt im Anschluss die Tabelle geordnet neu
        System.out.println(delete + " wird entfernt");                                                                  //Ausgabe des zu löschenden Elements *ggf. Abfrage zum Abbruch
        for (int i = 0; i < list.size(); i++) {                                                                         //Durchlauf der "Tabellen-Zeilen" in der Liste
            if (list.get(i).get(0).equals(delete)) {                                                                    //Abfrage auf gesuchten Key
                list.remove(i);                                                                                         //entfernen des Eintrags aus der Liste
                break;
            }
        }
        try {
            CreateFile();                                                                                               //Überschreiben des Datei-Inhalts mit nur der Kopfzeile
        } catch (IOException e) {
            e.printStackTrace();
        }
        FileWriter csvWriter = null;                                                                                    //Initialisieren des FileWriter
        try {
            csvWriter = new FileWriter(dateiName, true);                                                         //um geordnete Liste (Keys) zu garantieren, wird csv-Datei überschrieben
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        for (int i = 1; i < list.size(); i++) {                                                                         //Durchlauf aller "Daten-Zeilen" der Liste (ohne Kopfzeile)
            StringBuilder newLine = new StringBuilder();                                                                //neuer StringBuilder um neue Zeile zu erzeugen
            newLine.append(typ).append(i);                                                                              //erster Eintrag ist der Key des Objekts (Klassen-Name und Nummer des Eintrags). Die Elemente sind geordnet eingetragen
            try {
                for (int j=1; j<(AttributeFinder().size()+1);j++) {                                                     //Durchlauf aller Public-Attribute der Listenzeile
                    newLine.append(";").append(list.get(i).get(j));
                }
                assert csvWriter != null;
                csvWriter.append(String.valueOf(newLine)).append("\n");                                                 //Übergabe des Eintrags an csvWriter
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
        try {
            assert csvWriter != null;
            csvWriter.flush();                                                                                          //Schreiben der Zeile
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        try {
            csvWriter.close();                                                                                          //Beenden des Datei-Zugriffs
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        this.ShowItems();                                                                                               //Anzeigen der neuen Inhalte der Tabelle
    }

}
