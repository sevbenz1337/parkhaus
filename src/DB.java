import java.util.HashMap;

public class DB {

    public Class<?> cls;                                                                                                //Klasse des zugehörigen Objects
    public String typ;                                                                                                  //Klassen-Name
    public WriteCSV wCSV;
    public HashMap hashMap;

    public DB(Class<?> cls) {
        this.typ = cls.getName();                                                                                       //Klassen-Name schreiben
        this.cls = cls;                                                                                                 //Klasse schreiben
        this.wCSV=new WriteCSV(cls);
    }

    public HashMap DB_config() {
        boolean exit = true;
        wCSV.FileExists();
        do  {
            System.out.println("\"0\" zur "+typ+"-Anzeige");
            System.out.println("\"1\" um "+typ+" hinzuzufügen");
            System.out.println("\"2\" um "+typ+" zu entfernen");
            System.out.println("\"3\" um "+typ+"-Konfiguration zu beenden");
            int input = Tools.intEingabe();
            switch (input) {
                case 0 -> wCSV.ShowItems();
                case 1 -> wCSV.AddItem();
                case 2 -> {
                    System.out.println("Gib den Namen des Elements ein, dass gelöscht werden soll.");
                    String Delete = Tools.stringEingabe();
                    wCSV.DeleteItem(Delete);
                }
                case 3 -> {
                    ObjectGenerator objects= new ObjectGenerator(cls);
                    hashMap = objects.mapInstances();
                    exit = false;
                }
            }
        }while(exit);
    return hashMap;
    }
}
